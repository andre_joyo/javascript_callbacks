function a(){
  console.log("Se ha ejecutado la función a");
}

function b(){
  console.log("Se ha ejecutado la función b");
}

a();
b();


function a2(){
  // Simulamos un tiempo de espera en la ejecución de la función a
  setTimeout( function(){
    console.log("Se ha ejecutado la función a2");
  }, 1000 );
}

function b2(){
  console.log("Se ha ejecutado la función b2");
}

a2();
b2();

function a3(callback){
  // Simulamos un tiempo de espera en la ejecución de la función a
  setTimeout( function(){
    console.log("Se ha ejecutado la función a3");
    callback();
  }, 1000 );
}

function b3(){
  console.log("Se ha ejecutado la función b3");
}

a3(b3);


console.log("5 casos de uso de Callbacks:");
console.log("1 addEventListener(): Al producirse un evento se ejecuta una función.");
console.log("2 Peticiones AJAX: Se realizan consultas de manera asincrona");
console.log("3 prompt(): El flujo de ejecución se detiene hasta que finalice el prompt.");
console.log("4 alert(): Lo mismo que el prompt");
console.log("5 Javascript es un lenguaje orientado a eventos, es decir, a generar acciones(callbacks) cuando éstos se producen");

